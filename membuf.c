#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/vmalloc.h>
#include <linux/string.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Marina Pereskokova");
MODULE_DESCRIPTION("Membuf - memory buffer");
MODULE_VERSION("0.1");

#define MIN(a,b) (((a)<(b))?(a):(b))

static int size = 1024;
static char* memory = NULL;
struct rw_semaphore rwsem;

int param_size_set(const char *val, const struct kernel_param *kp)
{
	char* new_memory;
	int old_size = size;

	int res = param_set_int(val, kp);
        if (res == 0) 
        {
                pr_info("membuf: new size = %d\n", size);
		
		down_write(&rwsem);

		new_memory = vmalloc(size);
		memset(new_memory, 0, size);
		memcpy(new_memory, memory, MIN(old_size, size));

		vfree(memory);
		memory = new_memory;

		up_write(&rwsem);

		return 0;
	}

	return res;
}

const struct kernel_param_ops param_size_ops = 
{
	.set = &param_size_set,
	.get = &param_get_int,
};
 
module_param_cb(size, &param_size_ops, &size, 0644);
MODULE_PARM_DESC(size, "Membuf memory size");

static ssize_t membuf_read(struct file *file, char __user *buf, size_t len, loff_t *off)
{
	unsigned long to_copy, not_copied, copied;

	down_read(&rwsem);

	pr_info("membuf read, offset %lld, size %lu\n", *off, len);
	
	if (*off >= size)
	{
		up_read(&rwsem);
		return 0;
	}

	to_copy = len;
	if (*off + len > size)
	{
		to_copy = size - *off;
	}

	not_copied = copy_to_user(buf, memory + *off, to_copy);
	copied = to_copy - not_copied;
	*off += copied;

	up_read(&rwsem);

	return copied;
}

static ssize_t membuf_write(struct file *file, const char __user *buf, size_t len, loff_t *off)
{
	unsigned long not_wrote, wrote;

	down_write(&rwsem);

	pr_info("membuf write, offset: %lld, size: %lu\n", *off, len);
	
	if (*off + len > size) 
	{
		up_write(&rwsem);
		return -ENOMEM;
	}
	
	not_wrote = copy_from_user(memory + *off, buf, len);
	wrote = len - not_wrote;

	up_write(&rwsem);

	return wrote;
}

static struct file_operations membuf_ops =
{
	.owner      = THIS_MODULE,
	.read       = membuf_read,
	.write      = membuf_write,
};

dev_t dev = 0;
static struct cdev membuf_cdev;
static struct class *membuf_class;

static int __init membuf_start(void)
{
	int res;

	if ((res = alloc_chrdev_region(&dev, 0, 1, "membuf")) < 0)
	{
		pr_err("Error allocating major number\n");
		return res;
	}
	pr_info("membuf load: Major = %d Minor = %d\n", MAJOR(dev), MINOR(dev));
        
	cdev_init (&membuf_cdev, &membuf_ops);        
	if ((res = cdev_add (&membuf_cdev, dev, 1)) < 0)
        {
		pr_err("membuf: device registering error\n");
		unregister_chrdev_region (dev, 1);
		return res;
	}        
        
	if (IS_ERR(membuf_class = class_create(THIS_MODULE, "membuf_class")))
	{
		cdev_del(&membuf_cdev);
		unregister_chrdev_region (dev, 1);
		return -1;
	}
	
	if (IS_ERR(device_create(membuf_class, NULL, dev, NULL, "membuf")))
	{
		pr_err("membuf: error creating device\n");
		class_destroy (membuf_class);
		cdev_del (&membuf_cdev);
		unregister_chrdev_region(dev, 1);
		return -1;
	}
	pr_info("membuf: load size=%d\n", size);
        init_rwsem(&rwsem);

	down_write(&rwsem);

	memory = vmalloc(size);
	memset(memory, 0, size);

	up_write(&rwsem);

        return 0;
}

static void __exit membuf_end(void)
{
	device_destroy (membuf_class, dev);
	class_destroy (membuf_class);
	cdev_del (&membuf_cdev);
	unregister_chrdev_region(dev, 1);
	
	down_write(&rwsem);
	vfree(memory);
	up_write(&rwsem);

	pr_info("membuf: unload\n");
}

module_init(membuf_start);
module_exit(membuf_end);

